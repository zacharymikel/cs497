## LaraTweet - A CS497 Mini-Project ##
### Author - Zachary Mikel ###

This project is a simple example of crud operations with Laravel in PHP. 
There is full user management, registration, password resets and auth. 

This project is pretty bare-bones, there isn't a lot besides being able 
to tweet, modify a tweet, and delete. Due to time constraints, I wasn't 
able to get SASS or front-end minification set up. Deploying the application
on Heroku was pretty straightforward. 

The application uses a MySQL database with the ClearDB addon. 
