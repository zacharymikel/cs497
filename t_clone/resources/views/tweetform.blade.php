@component('tweetform')

    <div class="dashboard-post dashboard-post-new-tweet">
        <form id="tweet-form" class="form-horizontal" method="POST" action="{{ url('tweet') }}">
            {{ csrf_field() }}

            <div class="row tweet-box">
                <textarea id="content" type="text" placeholder="Wat?" name="content" maxlength="140" autofocus></textarea>

                <div id="tweet-error" class="col-sm-9 tweet-box">
                    <span id="tweet-error-message"></span>
                </div>

                <div class="col-sm-3 text-right no-padding">
                    <span id="counter" class="counter">140</span>
                    <button id="tweet-button" type="submit" class="btn btn-primary">twit</button>
                </div>
            </div>
        </form>
    </div>

@endcomponent
