@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="text-center">
                <button class="btn btn-primary">User Mode</button>
                <button class="btn btn-default">Presidential Mode</button>
            </div>

            <br />

            <div class="panel panel-default">

                <div class="panel-heading">Feed</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="dashboard-post dashboard-post-new-tweet">
                        <form id="tweet-form" class="form-horizontal" method="POST" action="{{ url('tweet') }}">
                            {{ csrf_field() }}

                            <div class="row tweet-box">
                                <input name="id" id="id" type="text" hidden>
                                <textarea id="content" type="text" placeholder="Wat?" name="content" maxlength="140" autofocus></textarea>

                                <div id="tweet-error" class="col-sm-9 tweet-box">
                                    <span id="tweet-error-message"></span>
                                </div>

                                <div class="col-sm-3 text-right no-padding">
                                    <span id="counter" class="counter">140</span>
                                    <button id="tweet-button" type="submit" class="btn btn-primary">twit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <span class="spacer"></span>

                    @foreach($tweets as $tweet)
                        <div class="dashboard-post tweet">

                            <div class="row">
                                <div class="col-sm-9"><span> {{ $tweet->user_name }} </span></div>
                                    {{csrf_field()}}
                                    <div class="col-sm-3 text-right">
                                        <input name="id" class="tweet-id" type="hidden" value="{{$tweet->id}}">
                                        <a class="action" id="edit-{{$tweet->id}}"><span class="glyphicon glyphicon-pencil pad"></span></a>
                                        <a class="action" id="delete-{{$tweet->id}}"><span class="glyphicon glyphicon-remove"></span></a>
                                    </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <p id="{{$tweet->id}}-static-content"> {{ $tweet->content }}</p>
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
