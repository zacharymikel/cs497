$(document).ready(function() {

    var url = "/";

    var actions = [
        "tweet"
    ];



    function setTweetButtonText() {
        $('#tweet-button').text(actions[getRandomInt(0, 4)]);
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    setTweetButtonText();

    $(".submit-edit").on('click', function() {

    });

    $(".cancel-edit").on('click', function() {
        var id = $(this).siblings('.tweet-id')[0].value;
        $("#" + id + "-content").addClass("hidden");
        $("#" + id + "-static-content").removeClass("hidden");
    });



    $(".action").on('click', function() {
        var elem = $(this)[0];
        var id = $(this).siblings('.tweet-id')[0].value;
        var content = $('#' + id + "-static-content")[0].textContent;

        if(elem.id.includes("edit")) {
            $('#tweet-form').find('#id')[0].value = id;
            $('#tweet-form').find('#content')[0].value = content;
        }

        else if(elem.id.includes("delete")) {
            sendRequest("DELETE", id, "tweet/");
        }
    });
});

// Tweet Counter
document.getElementById('content').onkeyup = function () {
    document.getElementById('counter').innerHTML = (140 - this.value.length);
};



// New & Edit Tweet
$(document).on('submit', '#tweet-form', function(e) {
    var isValid = validateTweet();
    if(!isValid) {
        e.preventDefault(); //prevent the default action
    }
});

// Validating Tweet
function validateTweet() {
    var content =  document.getElementById('content').value;
    if(content.length <= 0) {
        $('#tweet-error-message').text("You can't tweet nothing");
        return false;
    }
    return true;
}


function sendRequest(type, id, url) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: type,
        url: url + id,
        success: function(data) {
            window.location.href = "/";
        },
        error: function() {}
    });
}

