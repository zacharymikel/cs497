<?php

namespace App\Http\Controllers;
use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $tweets = $this->getByUserId($user_id);
        return view('home', ['tweets' => $tweets, 'user_id' => $user_id]);
    }

    public function getByUserId($user_id) {
        return Tweet::where('user_id', $user_id)->get();
    }

}
