<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'content', 'user_id', 'user_name'
    ];

}
