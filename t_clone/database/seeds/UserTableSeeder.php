<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: zacharymikel
 * Date: 9/30/17
 * Time: 4:22 PM
 */

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name'     => 'Zachary Mehkel',
            'username' => 'zacharymikel',
            'email'    => 'zacharymikel@u.boisestate.edu',
            'password' => Hash::make('root'),
        ]);
    }

}
