<?php

use App\Tweet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: zacharymikel
 * Date: 9/30/17
 * Time: 4:22 PM
 */

class TweetTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('tweets')->delete();
        for($i = 0; $i < 100; $i++) {
            DB::table('tweets')->insert([
                'user_name' => "zacharymikel",
                'content' => str_random(140),
                'created_at' => date("Y-m-d"),
                'user_id' => 22
            ]);
        }

    }

}
